### 안드로이드 구글 플레이 위치 API

> 구글 공식 Document와 샘플앱을 통해 작성 함.

* 구글 공식문서 https://developer.android.com/training/location/receive-location-updates?hl=ko
를 참고.

* '백그라운드 위치 액세스' 방식이 아닌 '사용자 시작 작업 지속하기' 방식.

* 앱이 백그라운드로 넘어가도 위치 업데이트는 포그라운드로 수행.  



**AndriodManifest.xml**  
~~~xml
<!-- Required for foreground services on P+. -->
<uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
<!-- 가장 정확도 높게 -->
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
~~~

~~~xml
<!-- Foreground services in Q+ require type. -->
<service
    android:name=".LocationUpdatesService"
    android:enabled="true"
    android:exported="true"
    android:foregroundServiceType="location" />
~~~



**LocationUpdatesService.java**  
~~~java

/**
 * 위치 업데이트 기준 주기.
 */
private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 20000L;

/**
 * 이 주기보다 빨리 갱신하지 않음.
 */
private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
        UPDATE_INTERVAL_IN_MILLISECONDS / 2;

/**
 * setInterval() 주기에 따라 취합된 여러 위치를 한번에 전달.
 * 하드웨어 기능에 따라 배터리를 덜 소모하고 정확한 위치를 제공할 수 있음.
 * 즉각적인 위치 전달이 필요하지 않은경우 가능한 큰 값을 설정하는게 유리함.
 */
private static final long MAX_WAIT_TIME_IN_MILLISECONDS = 
        UPDATE_INTERVAL_IN_MILLISECONDS * 2;

/**
 * 위치 업데이트에 필요한 최소 이동 거리(미터)
 */
private static final float SMALLEST_DISPLACEMENT_IN_METERS = 20.0F;

/**
 * 정확도, 성능에 영향을 미치는 LocationRequest 설정. API 문서 확인.
 * https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest?hl=ko
*/
private void createLocationRequest() {
    mLocationRequest = new LocationRequest();
    mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
    mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
    mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME_IN_MILLISECONDS);
    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    mLocationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT_IN_METERS);
}
~~~